package com.apitechu.apitechu.controllers;

import com.apitechu.apitechu.ApitechuApplication;
import com.apitechu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

  static final String APIBaseurl = "/apitechu/v1";

    @GetMapping(APIBaseurl + "/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseurl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("El id es " + id);

        ProductModel result = new ProductModel();

        for(ProductModel product: ApitechuApplication.productModels){
            if(product.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = product;
            }
        }
        return result;
    }

    @PostMapping(APIBaseurl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct ){
        System.out.println("Estoy en createProduct");
        System.out.println("Id del producto: " + newProduct.getId());
        System.out.println("Precio del producto: " + newProduct.getPrice());
        System.out.println("Descripcion del producto:  "+newProduct.getDesc());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseurl+"/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");

        for (ProductModel productV: ApitechuApplication.productModels) {
            if (productV.getId().equals(id)){
                productV.setDesc(product.getDesc());
                productV.setPrice((int) product.getPrice());
            }
        }

        return product;
    }

    @PatchMapping(APIBaseurl + "/products/{id}")
    public ProductModel parcialUpdateProducts(@RequestBody ProductModel product,@PathVariable String id){
        System.out.println("Metodo path actualizacion pacial");
        System.out.println("datos entrada: " + product.getDesc() + " : " + product.getPrice() );


        for(ProductModel productP: ApitechuApplication.productModels){
            if(productP.getId().equals(id)){
                if(product.getPrice() > 0){
                    productP.setPrice((int)product.getPrice());
                }

                if(product.getDesc() != null) {
                    productP.setDesc(product.getDesc());
                }
            }

        }

        return product;

    }

    @DeleteMapping(APIBaseurl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("Delete Product");
        boolean found = false;
        ProductModel result = new ProductModel();

        for (ProductModel productF :ApitechuApplication.productModels){
            if(productF.getId().equals(id)){
                found = true;
                result = productF;
            }
        }

        if(found){
            ApitechuApplication.productModels.remove(result);
        }
        return new ProductModel();

    }
}
